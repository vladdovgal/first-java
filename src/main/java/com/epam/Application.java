package com.epam;

/**
 * <h1>Just trying</h1>
 * This comment exists only for saying <b>"Hello"</b>
 * @author Vladyslav Dovgal
 * @version 4.2.0
 * @since 19.03.2020
 */
public class Application {
    public static void main(String[] args) {
        System.out.println("Hello");
    }
}
